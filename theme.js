// import { storage } from './storage';
const $body = document.querySelector('body');
const $button = document.getElementById('button-theme');

const theme = () => {
  document.addEventListener('DOMContentLoaded', (even) => {
    load();
  });

  $button.addEventListener('click', () => {
    $body.classList.toggle('dark-mode-off');
    storage.asignar('darkmode', $body.classList.contains('dark-mode-off'));
  });

  const load = () => {
    const darkMode = storage.obtener('darkmode');
    if (!darkMode) {
      storage.asignar('darkmode', 'false');
    } else if (darkMode == 'true') {
      $body.classList.add('dark-mode-off');
    }
  };
};
// local storage
const storage = {
  obtener(key) {
    const value = localStorage.getItem(key);
    if (!value) {
      return null;
    }
    return value;
  },
  asignar(key, value) {
    localStorage.setItem(key, value);
  },
  eliminar(key) {
    localStorage.removeItem(key);
  },
  limpiar() {
    localStorage.clear();
  },
};
export { theme };
